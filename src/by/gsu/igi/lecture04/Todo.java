package by.gsu.igi.lecture04;

public @interface Todo {
    String value();
}
