package by.gsu.igi.lecture04;

public abstract class Animal {
    private int age;

    public int getAge() {
        return age;
    }

    public void becomeOlder() {
        age++;
    }
}
