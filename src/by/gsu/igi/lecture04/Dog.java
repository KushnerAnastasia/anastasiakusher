package by.gsu.igi.lecture04;

/**
 * This class represents any <b>dog</b> in the world.
 * See the {@link by.gsu.igi.lecture01.NewHelloWorld#main(String[])}
 *
 * @author Anton
 * @version 21213
 * @see by.gsu.igi.lecture02.StaticDemo
 * @since version 123
 */
public class Dog extends Animal {
    String name;

    /**
     * Creates a dog with a name
     *
     * @param name specifies the new name
     */
    public Dog(String name) {
        this.name = name;
    }

    public Dog rename(String newName) {
        return new Dog(newName);
    }

    public String toString() {
        return "Dog named " + name;
    }
}
